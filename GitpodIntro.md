# Gitpod Introduction

We will be using [Gitpod](https://www.gitpod.io) as our development
environment because it runs in the cloud and does not require a local
computer with Docker and significant resources.

Gitpod will provide us with Visual Studio Code in the browser. But this will
also require being able to access projects on Gitlab.

This activity will provide you practice with getting a copy of a project on
GitLab (putting the copy into your team's GitLab subgroup or your own 
GitLab subgroup for submitting homework assignments) and then working in the
project in Gitpod.

## Do this as a team. Choose one person to display their screen while doing this

### Create a Free Gitpod Account

1. Go to [https://gitpod.io/login](https://gitpod.io/login) and create a
free account.

### Gitpod Browser Extension

Gitpod provides a browser extension for Chrome or Firefox that adds a button
on the GitLab page in your browser.

1. Install the [Gitpod Browser Extension](https://www.gitpod.io/docs/configure/user-settings/browser-extension)

If you are not using Chrome or Firefox, you can still open GitLab projects
in your browser, but you will have to modify the URL to the project. You will
have to add `https://gitpod.io/#` at the beginning of the GitLab project URL.

### Fork this activity into your team's subgroup

You cannot make changes to the class copy of this project. You need to make
a *fork* of this project in a subgroup where you have the ability to make
changes.

1. Open this project's GitLab page in your browser: [https://gitlab.com/worcester/cs/cs-443-01-02-spring-2024/in-class-activities/gitpod-intro](https://gitlab.com/worcester/cs/cs-443-01-02-spring-2024/in-class-activities/gitpod-intro)
2. Click on the `Fork` button on the right-hand side (above Project
Information).
3. Under `Project URL` in the namespace dropdown choose your team's subgroup
it will look like `worcester/cs/cs-443-01-02-spring-2024/section-01/team-1`.
(Be sure to choose the correct section and team number.)
4. Click the `Fork project` button.

When it finished, you will be on the GitLab page for your fork. Verify this
by looking at the address.

### Open the fork in Gitpod

1. Click on the `G Open` button to open the project in Gitpod.
    Or, paste `https://gitpod.io/#` at the beginning of the address in the
address bar.
2. Log in to GitLab is asked to do so.
3. When the `New Workspace` page is shown, click `Continue`.
4. You will see the project in the browser version of Visual Studio Code.

### Run and modify the Hello.java Program

1. Right-click on the `Hello.java` program, and choose `Run Java`. It may
take some time for VS Code to load extensions and make this option available.
2. View the output in the Terminal window.
3. Change the output of your program to include your team number.
4. Save the file.
5. Run the program again.

### Save Your Changes to GitLab

Your changes only exist in your Gitpod workspace at this point. The
workspace will be deleted after 14 days of not using it.

So, we should save your changes to your team's fork.

1. In your terminal type the following to add, commit, and push your work

    ```
    git add .
    git commit -m"modified Hello.java`
    git push
    ```

    Your changes will now be on GitLab. 
    
2. Go to your fork on GitLab and refresh your browser. Verify that your
changes are there.

### Stop your Gitpod Workspace

You do not want to keep your Gitpod workspace running so that you can save
your free time. The workspace will shut down after 30 minutes of inactivity
but you can stop it immediately.

1. In your terminal type `gp stop`.

## Do the same steps individually

**Ask your teammates for help if needed.**

Each of you will want to be able to do the same thing in your own GitLab
subgroup for homework.

1. Create a Free Gitpod Account
2. Install the Gitpod Browser Extension
3. Fork this activity into **your own homework subgroup**

    You each have a GitLab subgroup under this course for your homework
    assignments. **Only you and your instructor have access to this subgroup.**

    1. Open this project's GitLab page in your browser: [https://gitlab.com/worcester/cs/cs-443-01-02-spring-2024/in-class-activities/gitpod-intro](https://gitlab.com/worcester/cs/cs-443-01-02-spring-2024/in-class-activities/gitpod-intro)
    2. Click on the `Fork` button on the right-hand side (above Project
    Information).
    3. Under `Project URL` in the namespace dropdown choose **your homework**
    subgroup it will look like 
    `worcester/cs/cs-443-01-02-spring-2024/students/your-wsu-username`.
    4. Click the `Fork project` button.

    When it finished, you will be on the GitLab page for your fork. Verify this
    by looking at the address.

4. Open the fork in Gitpod
5. Run and modify the Hello.java Program - add your name to the output.
6. Save Your Changes to GitLab
7. Stop your Gitpod Workspace

&copy; 2024 Karl R. Wurst, Worcester State University

<img src="http://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/]() or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA
